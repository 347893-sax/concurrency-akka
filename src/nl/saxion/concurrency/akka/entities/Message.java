package nl.saxion.concurrency.akka.entities;

/**
 * Contributors:
 * yourilefers
 *
 * @since v1.0
 */
public enum Message {

    // Message to start a piet
    START,

    // Work is done by a collector piet
    WORK_DONE_COLLECTING,

    // Work is done by a working piet
    WORK_DONE_WORKING,

    // An invitation for the collect meeting
    INVITATION_COLLECT_MEETING,

    // An invitation for the work meeting
    INVITATION_WORK_MEETING,

    // Accept an invitation
    INVITATION_OK,

    // Meeting is done
    MEETING_DONE

}
