package nl.saxion.concurrency.akka.threads.pieten;

import akka.actor.UntypedActor;
import nl.saxion.concurrency.akka.Program;

/**
 * Abstract class for a piet.
 */
public abstract class Piet extends UntypedActor {

	/** The name */
	protected String name;

	/** The color */
	protected String color;

	/** The program instance */
	protected Program program;

	/** Meeting state of the piet */
	protected boolean inMeeting = false;

	//
	// Methods
	//

	/**
	 * Constructor for a piet.
	 *
	 * @param name  The name of the piet
	 * @param color The color of the piet
	 */
	public Piet(String name, String color, Program program){

		// Set vars
		this.name = name;
		this.color = color;
		this.program = program;
		
	}

	/**
	 * Get the color of the piet.
	 *
	 * @return
	 */
	public String getColor(){
		return color;
	}

	/**
	 * Override toString for piet.
	 *
	 * @return
	 */
	@Override
	public String toString(){
		return this.getClass().getSimpleName() + " (" + this.name + ") (" + color + ")";
	}

	/**
	 * Override preStart()
	 *
	 * @throws Exception
	 */
	@Override
	public void preStart() throws Exception {
		this.program.getPieten().put(this.getSelf(), this);
		System.out.println(this + " started...");
	}

	/**
	 * Override postStop()
	 *
	 * @throws Exception
	 */
	@Override
	public void postStop() throws Exception {
		System.out.println(this + " stopped...");
	}

	/**
	 * Unhandled method
	 * @param message
	 */
	protected void _unhandled(Object message) {
		System.out.println(this + " | Received unknown message! : " + message);
		super.unhandled(message);
	}
}
