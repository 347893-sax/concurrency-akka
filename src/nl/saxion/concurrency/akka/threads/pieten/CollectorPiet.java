package nl.saxion.concurrency.akka.threads.pieten;

import nl.saxion.concurrency.akka.Program;
import nl.saxion.concurrency.akka.entities.Message;

/**
 * The collector piet thread.
 *
 * The collector piet will collect stuff.
 */
public class CollectorPiet extends Piet {

	//
	// Construction
	//

	/**
	 * Constructor for the collector piet.
	 *
	 * @param name
	 * @param color
	 */
	public CollectorPiet(String name, String color, Program program){
		super(name, color, program);
	}

	/**
	 * Inside the method the collector piet will collect lists.
	 */
	private void collect() throws InterruptedException {
		System.out.println(this + " | Collect");
		Thread.sleep((int) (Math.random() * 1000));
		System.out.println(this + " | Collecting done");
		this.program.getAdminPiet().tell(Message.WORK_DONE_COLLECTING, getSelf());
	}

	/**
	 * Called when a message is received.
	 *
	 * @param message
	 * @throws Exception
	 */
	@Override
	public void onReceive(Object message) throws Exception {
		System.out.println(this + " | Got message: " + message);

		// If is message
		if(message instanceof Message) {
			Message m = (Message) message;

			// Switch for the message
			switch(m) {
				case START:
					collect();
					break;

				case INVITATION_COLLECT_MEETING:
					this.inMeeting = true;
					this.getSender().tell(Message.INVITATION_OK, this.getSelf());
					break;

				case MEETING_DONE:
					this.inMeeting = false;
					collect();
					break;

				default: _unhandled(message);
			}
		}
	}

}
