package nl.saxion.concurrency.akka.threads.pieten;

import nl.saxion.concurrency.akka.Program;
import nl.saxion.concurrency.akka.entities.Color;
import nl.saxion.concurrency.akka.entities.Message;

/**
 * The work piet class.
 *
 * This piet will work and have some a ...
 */
public class WorkPiet extends Piet {

	//
	// Methods
	//

	/**
	 * Constructor for the workpiet
	 *
	 * @param name
	 * @param color
	 */
	public WorkPiet(String name, String color, Program program){
		super(name, color, program);
	}

	/**
	 * Inside the method the work piet will work.
	 */
	private void work() throws InterruptedException {
		System.out.println(this + " | Werken");
		Thread.sleep((int) (Math.random() * 1000));
		System.out.println(this + " | Werken done");
		this.program.getAdminPiet().tell(Message.WORK_DONE_WORKING, getSelf());
	}

	/**
	 * Called when a message is received.
	 *
	 * @param message
	 * @throws Exception
	 */
	@Override
	public void onReceive(Object message) throws Exception {
		System.out.println(this + " | Got message: " + message);

		if(message instanceof Message) {
			Message m = (Message) message;

			// Switch for the message
			switch(m) {
                case MEETING_DONE:
                    this.inMeeting = false;
                case START:
					work();
					break;

				case INVITATION_COLLECT_MEETING:
					if(this.color.equals(Color.BLACK)) {
						this.inMeeting = true;
						this.getSender().tell(Message.INVITATION_OK, this.getSelf());
					}
					break;

				case INVITATION_WORK_MEETING:
					this.inMeeting = true;
					this.getSender().tell(Message.INVITATION_OK, this.getSelf());
					break;

				default: _unhandled(message);
			}
		}
	}

}
