package nl.saxion.concurrency.akka.threads.pieten;

import akka.actor.ActorRef;
import nl.saxion.concurrency.akka.Program;
import nl.saxion.concurrency.akka.entities.Color;
import nl.saxion.concurrency.akka.entities.Message;

import java.util.ArrayList;

/**
 * Contributors:
 * yourilefers
 *
 * @since v1.0
 */
public class AdminPiet extends Piet {

    /** The number collector pieten that are done */
    private ArrayList<ActorRef> numberOfDoneCollectorPieten;

    /** The number work pieten that are done */
    private ArrayList<ActorRef> numberOfDoneWorkPieten;

    /** The number black work pieten that are done */
    private ArrayList<ActorRef> numberOfDoneBlackPieten;

    /** Is a meeting running right now or not */
    private boolean meetingGoing = false;

    //
    // Methods
    //

    /**
     * Constructor for the administrator piet.
     *
     * @param name
     * @param color
     */
    public AdminPiet(String name, String color, Program program){
        super(name, color, program);

        // Create array list
        this.numberOfDoneCollectorPieten = new ArrayList<ActorRef>();
        this.numberOfDoneWorkPieten = new ArrayList<ActorRef>();
        this.numberOfDoneBlackPieten = new ArrayList<ActorRef>();

    }

    /**
     * Override preStart to start all pieten.
     */
    public void startPieten() {

        //
        // Run instances
        //

        // Start pieten
        for(ActorRef a : this.program.getPieten().keySet()) {
            a.tell(Message.START, this.getSelf());
        }

    }

    //
    // Con methods
    //

    /**
     * Called when a message is received.
     *
     * @param message
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        System.out.println(this + " | Got message: " + message);

        if(message instanceof Message) {
            Message m = (Message) message;

            // Switch for the message
            switch(m) {
                case WORK_DONE_WORKING:
                    receivedWorkDoneWorking();
                    break;

                case WORK_DONE_COLLECTING:
                    receivedWorkDoneCollecting();
                    break;

                case MEETING_DONE:
                    meetingGoing = false;
                    checkMeeting();
                    break;

                default: unhandled(message);
            }
        }
    }

    /**
     * Called when I received a 'work done' method.
     */
    private void receivedWorkDoneWorking() {

        // Get sender
        Piet p = this.program.getPiet(getSender());

        if(p instanceof WorkPiet && p.getColor().equals(Color.BLACK)) {

            // Add to list
            this.numberOfDoneBlackPieten.add(getSender());

        } else {

            // Add sender to done working
            this.numberOfDoneWorkPieten.add(this.getSender());

        }

        // Cannot start a meeting?
        if(!checkMeeting()) {

            // Sender may work again
            getSender().tell(Message.START, getSelf());

        }

    }

    /**
     * Called when I received a 'work done' method.
     */
    private void receivedWorkDoneCollecting() {

        // Add sender to done collecting
        this.numberOfDoneCollectorPieten.add(this.getSender());

        // Check for meeting
        checkMeeting();

    }

    /**
     * Check for a meeting
     * @return
     */
    private boolean checkMeeting() {
        return checkCollectorMeeting() || checkWorkMeeting();
    }

    /**
     * May a work meeting start?
     */
    private boolean checkWorkMeeting() {
        System.out.println("Work stats: wp done: " + numberOfDoneWorkPieten.size() + " | Sint: " + meetingGoing);

        // Check for meeting
        if(this.numberOfDoneWorkPieten.size() > 2 && !meetingGoing) {

            // Meeting starting...
            meetingGoing = true;

            // Sent message to sint to start the collect meeting
            this.program.getSint().tell(Message.INVITATION_WORK_MEETING, getSelf());

            // Create temporary list
            ArrayList<ActorRef> temp = new ArrayList<ActorRef>();

            // Sent pieten to work who may not be invited
            for(int i = 0; i < this.numberOfDoneWorkPieten.size(); i++) {
                if(i < Program.WACHTEN_WERKOVERLEG_PIETEN) {
                    temp.add(this.numberOfDoneWorkPieten.get(i));
                } else {
                    this.numberOfDoneWorkPieten.get(i).tell(Message.START, getSelf());
                }
            }

            // Clear the list
            this.numberOfDoneWorkPieten.clear();

            // Sent done collectorpieten to sint
            this.program.getSint().tell(temp, getSelf());

            return true;

        }

        return false;

    }

    /**
     * May a collector meeting start?
     */
    private boolean checkCollectorMeeting() {
        System.out.println("Collect stats: cp done: " + numberOfDoneCollectorPieten.size() + " | bp done: " + numberOfDoneBlackPieten.size() + " | Sint: " + meetingGoing);

        // Check for meeting
        if(this.numberOfDoneCollectorPieten.size() > 2 && this.numberOfDoneBlackPieten.size() > 0 && !meetingGoing) {

            // Meeting starting...
            meetingGoing = true;

            // Sent message to sint to start the collect meeting
            this.program.getSint().tell(Message.INVITATION_COLLECT_MEETING, getSelf());

            // The list to sent
            this.numberOfDoneCollectorPieten.add(this.numberOfDoneBlackPieten.get(0));
            this.numberOfDoneBlackPieten.remove(0);
            this.numberOfDoneWorkPieten.addAll(this.numberOfDoneBlackPieten);
            this.numberOfDoneBlackPieten.clear();

            // Make all work pieten go to work again
            for(ActorRef a : this.numberOfDoneWorkPieten) {
                a.tell(Message.START, getSelf());
            }
            this.numberOfDoneWorkPieten.clear();

            // Sent done collectorpieten to sint
            this.program.getSint().tell(this.numberOfDoneCollectorPieten.clone(), getSelf());
            this.numberOfDoneCollectorPieten.clear();

            return true;

        }

        return false;

    }

}
