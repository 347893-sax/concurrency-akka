package nl.saxion.concurrency.akka.threads;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import nl.saxion.concurrency.akka.entities.Message;

import java.util.ArrayList;

/**
 * Created by marc on 3-12-14.
 */
public class Sint extends UntypedActor {

	/** True when planning a work meeting */
	private boolean planningWorkMeeting = false;

	/** True when planning a collector meeting */
	private boolean planningCollectorMeeting = false;

	/** The actors in the meeting */
	private ArrayList<ActorRef> actorsInMeeting;

	/** The program instance */
	private ActorRef adminPiet;

	/** The number of acknowledgements retrieved */
	private int gotNumberOfAcknowledgements = 0;

	//
	// Methods
	//

	/**
	 * Constructor for the sint.
	 */
	public Sint(ActorRef adminPiett){

		// Get program instance
		adminPiet = adminPiett;
		actorsInMeeting = new ArrayList<ActorRef>();

	}

	/**
	 * Override toString for the object
	 * @return
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	/**
	 * Called when receiving a message.
	 * @param message
	 * @throws Exception
	 */
	@Override
	public void onReceive(Object message) throws Exception {
		System.out.println(this + " | Got message: " + message);

		if(message instanceof Message) {
			Message m = (Message) message;

			// Switch for the message
			switch(m) {
				case INVITATION_WORK_MEETING:
					planningWorkMeeting = true;
					break;

				case INVITATION_COLLECT_MEETING:
					planningCollectorMeeting = true;
					break;

				case INVITATION_OK:
					gotAcknowledgement();
					break;

				default: unhandled(message);
			}
		} else if(message instanceof ArrayList) {

			// Cast to the array list that is needed
			setupMeeting((ArrayList<ActorRef>) message);

		}
	}

	/**
	 * Called for settings up a meeting.
	 * @param pieten
	 */
	private void setupMeeting(ArrayList<ActorRef> pieten) {

		System.out.println(this + " | Settings up meeting...");

		// Are we going to plan something?
		if(!planningCollectorMeeting && !planningWorkMeeting) {
			return;
		}

		// Set as in meeting list
		actorsInMeeting = pieten;

		// Set number
		gotNumberOfAcknowledgements = 0;

		System.out.println(this + " | Sending invitations...");

		// Set invitations
		for(ActorRef a : actorsInMeeting) {
			if(planningCollectorMeeting) {
				a.tell(Message.INVITATION_COLLECT_MEETING, getSelf());
			} else {
				a.tell(Message.INVITATION_WORK_MEETING, getSelf());
			}
		}

		System.out.println(this + " | Sent invitations...");

	}

	/**
	 * Called when the sint got an acknowledgement
	 */
	private void gotAcknowledgement() {

		// Increase
		gotNumberOfAcknowledgements++;

		// Got all?
		if(gotNumberOfAcknowledgements >= actorsInMeeting.size()) {

			// Now we will keep the meeting
			overleg();

			// Meeting done
			planningCollectorMeeting = false;
			planningWorkMeeting = false;

			// Sent 'done'
			for(ActorRef a : actorsInMeeting) {
				a.tell(Message.MEETING_DONE, getSelf());
			}

			// Tell admin piet
			adminPiet.tell(Message.MEETING_DONE, getSelf());

		}

	}

	/**
	 * How to keep a 'overleg'
	 */
	private void overleg() {
		try {

			// Debug
			if (planningCollectorMeeting) System.out.print(this + " | VerzamelOverleg ");
			else System.out.print(this + " | WerkOverleg ");
			System.out.println("begonnen met #" + actorsInMeeting.size() + " personen");

			// Sleep
			Thread.sleep((int) (Math.random() * 1000));

			// Debug
			if (planningCollectorMeeting) System.out.print(this + " | VerzamelOverleg ");
			else System.out.print(this + " | WerkOverleg ");
			System.out.println("afgelopen");

		} catch(InterruptedException e) {

			// Debug
			if (planningCollectorMeeting) System.out.print(this + " | VerzamelOverleg ");
			else System.out.print(this + " | WerkOverleg ");
			System.out.println("interrupted: " + e.getLocalizedMessage());

		}
	}

}
