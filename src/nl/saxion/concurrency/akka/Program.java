package nl.saxion.concurrency.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import nl.saxion.concurrency.akka.entities.Color;
import nl.saxion.concurrency.akka.threads.Sint;
import nl.saxion.concurrency.akka.threads.pieten.AdminPiet;
import nl.saxion.concurrency.akka.threads.pieten.CollectorPiet;
import nl.saxion.concurrency.akka.threads.pieten.Piet;
import nl.saxion.concurrency.akka.threads.pieten.WorkPiet;

import java.util.HashMap;

/**
 * The main program class.
 *
 * This class is singleton and contains all important
 * variables. It will instantiate and start all instances,
 * including the threads.
 */
public class Program {

	/**
	 * The program starter.
	 *
	 * This method will start the program.
	 *
	 * @param args
	 */
	public static void main(String[] args){
		new Program().run();
	}

	//
	// Variables
	//

	/** The number of collector pieten */
	public static final int AANTAL_VERZAMEL_PIETEN = 6;

	/** The number of work pieten */
	public static final int AANTAL_WERK_PIETEN = 6;

	/** The size of werkoverleg */
	public static final int WACHTEN_WERKOVERLEG_PIETEN = 3;

	//
	// Private
	//

	/** The actor system */
	private ActorSystem system;

	/** De werkpieten en verzamelpieten */
	private HashMap<ActorRef, Piet> pieten;

	/** The actor piet */
	private ActorRef adminPiet, sint;

	//
	// Construction
	//

	/**
	 * Constructor of the program.
	 * Will setup all instances
	 */
	public Program(){

		// Create actor system
		this.system = ActorSystem.create("HiSystem");

		// Create lists
		this.pieten = new HashMap<ActorRef, Piet>();

	}

	//
	// Methods
	//

	/**
	 * This method will run the program.
	 *
	 * First it wil create the sint and the pieten.
	 * After this all threads will be started.
	 */
	public void run() {

		//
		// Creation
		//

		// Create collector pieten
		for(int i = 0; i < AANTAL_VERZAMEL_PIETEN; i++) {
			this.system.actorOf(Props.create(CollectorPiet.class, "cp" + i, Color.randomColor(), this), "HiCollectPiet" + i);
		}

		// Create work pieten
		for( int i = 0; i < AANTAL_WERK_PIETEN - 1; i++) {
			this.system.actorOf(Props.create(WorkPiet.class, "wp" + i, Color.randomColor(), this), "HiWorkPiet" + i);
		}
		// Make sure we have at least 1 black piet and admin piet
		this.system.actorOf(Props.create(WorkPiet.class, "wp" + (AANTAL_WERK_PIETEN - 1), Color.BLACK, this), "HiWorkPiet" + (AANTAL_WERK_PIETEN - 1));
		this.adminPiet = this.system.actorOf(Props.create(AdminPiet.class, "ap", Color.randomColor(), this), "HiAdminPiet");

		// Create the sint
		this.sint = this.system.actorOf(Props.create(Sint.class, this.adminPiet), "HiSint");

		// Start pieten
		((AdminPiet) this.getPiet(this.adminPiet)).startPieten();

	}

	/**
	 * Get the list of pieten.
	 *
	 * @return
	 */
	public HashMap<ActorRef, Piet> getPieten() {
		return this.pieten;
	}

	/**
	 * Get the Sint instance.
	 *
	 * @return
	 */
	public ActorRef getSint() {
		return this.sint;
	}

	/**
	 * Get the admin piet.
	 *
	 * @return
	 */
	public ActorRef getAdminPiet() {
		return this.adminPiet;
	}

	/**
	 * Get a piet by the actor reference.
	 *
	 * @param p
	 * @return
	 */
	public Piet getPiet(ActorRef p) {
		return this.pieten.get(p);
	}

}
